# README #

### What is this repository for? ###

* RestAsuured Twitter Api tests (with implementation oauth 1.0 class to generate authorized header) 
* Version : 1.0

### How do I get set up? ###

* install maven 3 && JDK 1.8 
* insert in Oauth.class , from https://apps.twitter.com/, keys & access tokens 

### How to run? ###

mvn test

### Where is reports? ###

${project.build.directory}/reports/index.html

by default : targets/reports/index.html