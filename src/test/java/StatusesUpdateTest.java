import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.path.json.JsonPath;
import org.testng.Reporter;
import org.testng.annotations.Test;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static org.hamcrest.Matchers.*;

import static io.restassured.RestAssured.given;

@Test(suiteName = "/statuses/update", description = "https://dev.twitter.com/rest/reference/post/statuses/update")
public class StatusesUpdateTest {

    private OAuth oAuth = new OAuth();
    private String method = "post";
    private String URL = "https://api.twitter.com/";
    private String URN = "/1.1/statuses/update.json";
    private String URI = "https://api.twitter.com/1.1/statuses/update.json";

    private String status = oAuth.generateNonce();

    @Test(description = "send request without authorized headers. Expected -> 400, Error_code = 215")
    public void sendRequestWithoutAuthorizedHeaderTest() {
        given().log().all(true)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .expect()
                .statusCode(400)
                .body("errors[0].code", comparesEqualTo(215))
                .body("errors[0].message", equalTo("Bad Authentication data."))
                .when()
                .post(URI);

    }

    @Test(description = "send request with wrong authorized headers. Expected -> 401, Error_code = 32")
    public void sendPostWithWrongAuthorizedHeaderTest() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        Map<String, String> params = new HashMap<>();
        params.put("status", status);

        Header header = new Header("Authorization", oAuth.getHeader(method, URI, params));

        given().log().all(true)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .header(header)
                .expect()
                .statusCode(401)
                .body("errors[0].code", comparesEqualTo(32))
                .body("errors[0].message", equalTo("Could not authenticate you."))
                .when()
                .get(URI);

    }

    @Test(description = "post status. Expected -> 200, post status == response status")
    public void postStatusTest() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {

        Map<String, String> params = new HashMap<>();
        params.put("status", status);

        Header header = new Header("Authorization", oAuth.getHeader(method, URI, params));
        given().log().all(true)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .header(header)
                .params(params)
                .expect()
                .log().all(true)
                .statusCode(200)
                .body("text", equalTo(status))
                .when()
                .post(URI);

    }

    @Test(description = "post status. Expected -> 200, post long, lat params == response long, lat params")
    public void postStatusWithGeoTest() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {

        String contextStatus = oAuth.generateNonce();

        Map<String, String> params = new HashMap<>();
        params.put("status", contextStatus);
        params.put("long", "-122.40061283");//-180.0 to +180.0
        params.put("lat", "37.782112"); //-90.0 to +90.0
//        params.put("display_coordinates", "true");

        Header header = new Header("Authorization", oAuth.getHeader(method, URI, params));

        given().log().all(true)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .header(header)
                .params(params)
                .expect()
                .log().all(true)
                .statusCode(200)
                .body("text", equalTo(contextStatus))
                .body("geo.coordinates[1]", comparesEqualTo(-122.40061283f))
                .body("geo.coordinates[0]", comparesEqualTo(37.78211206f))
                .body("coordinates.coordinates[0]", comparesEqualTo(-122.40061f))
                .body("coordinates.coordinates[1]", comparesEqualTo(37.782112f))
                .when()
                .post(URI)
                .prettyPrint();

    }
    @Test(description = "Try post status with >140 characters. Expected -> 403, Error_code = 186")
    public void postStatusWithWrongQuantityOfCharactersTest() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {

        Reporter.log("Message", true);

        Map<String, String> params = new HashMap<>();
        params.put("status",
                "hfkjsdahfasdklfhdsfjkhdsafhuasdhfudashfhdafhuihdasifffidsahfuhsdfhiuadshfu " +
                        "dsaiufhiuadshfiuhuidahfuisdahfuihdaiuhfiudhsafhduiahfiuhasdiuhfudud"); //141 caracters

        Header header = new Header("Authorization", oAuth.getHeader(method, URI, params));

        given().log().all(true)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .header(header)
                .params(params)
                .expect()
                .statusCode(403)
                .body("errors[0].code", comparesEqualTo(186))
                .body("errors[0].message", equalTo("Status is over 140 characters."))
                .when()
                .post(URI)
                .andReturn()
                .prettyPrint();

    }

    @Test(dependsOnMethods = "postStatusTest", description = "Try post duplicate status. Expected -> 403, Error_code = 187")
    public void postDuplicateTest() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {

        Map<String, String> params = new HashMap<>();
        params.put("status", status);

        Header header = new Header("Authorization", oAuth.getHeader(method, URI, params));

//        JsonPath error = new JsonPath("$.errors[:1].code");
        given().log().all(true)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .header(header)
                .params(params)
                .expect()
                .statusCode(403)
                .body("errors[0].code", comparesEqualTo(187))
                .body("errors[0].message", equalTo("Status is a duplicate."))
                .when()
                .post(URI);

    }

    @Test(description = "request return trim user(contains only id && id_str). Expected -> 200, user.keySet().size() == 2 ")
    public void trimUserTest() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        String contextStatus = oAuth.generateNonce();
        Map<String, String> params = new HashMap<>();
        params.put("status", contextStatus);
        params.put("trim_user", "true");

        Header header = new Header("Authorization", oAuth.getHeader(method, URI, params));
        given().log().all(true)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .header(header)
                .params(params)
                .expect()
                .log().all(true)
                .statusCode(200)
                .body("user", hasKey("id"))
                .body("user", hasKey("id_str"))
                .body("user.keySet().size()", is(2))
                .when()
                .post(URI);

    }
}
