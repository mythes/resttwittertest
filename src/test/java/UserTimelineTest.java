
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import org.testng.annotations.Test;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.when;
import static org.hamcrest.Matchers.*;

@Test(suiteName = "/statuses/user_timeline")
public class UserTimelineTest {

    private OAuth oAuth = new OAuth();
    private String method = "get";
    private String URL = "https://api.twitter.com/";
    private String URN = "1.1/statuses/user_timeline.json";
    private String URI = "https://api.twitter.com/1.1/statuses/user_timeline.json";


//    "user_id","12345"
//    "screen_name","twitterapi"
//    "count","2"
//    "include_rts","false"
//    "since_id","12345"
//    "max_id","54321"
//    "trim_user","true"
//    "exclude_replies","false"
//    "include_rts","true"

    @Test(description = "send request without authorized headers. Expected -> 400, Error_code = 215")
    public void sendRequestWithoutAuthorizedHeaderTest() {
        given().log().all(true)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .get(URI)
                .then()
                .statusCode(400)
                .body("errors[0].code", comparesEqualTo(215))
                .body("errors[0].message", equalTo("Bad Authentication data."))
                .log().all(true);
    }

    @Test(description = "send request with wrong authorized headers. Expected -> 401, Error_code = 32")
    public void sendRequestWithWrongAuthorizedHeaderTest() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        Map<String, String> params = new HashMap<>();
        params.put("screen_name", "twitterapi");

        Header header = new Header("Authorization", oAuth.getHeader(method, URI, params));

        given().log().all(true)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .header(header)
                .get(URI)
                .then()
                .statusCode(401)
                .body("errors[0].code", comparesEqualTo(32))
                .body("errors[0].message", equalTo("Could not authenticate you."))
                .log().all(true);
    }

    @Test(description = "request without parameters. Expected -> 200")
    public void OkWithoutParamsTest() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {

        Header header = new Header("Authorization", oAuth.getHeader(method, URI, null));

        given().log().all(true)
                .baseUri(URL)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .header(header)
                .get(URN)
                .then()
                .statusCode(200)
                .log().all(true);
    }

    public void OkWithParamsTest() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {
        Map<String, String> params = new HashMap<>();
        params.put("screen_name", "twitterapi");
        params.put("count", "3");

        Header header = new Header("Authorization", oAuth.getHeader(method, URI, params));

        given().log().all(true)
                .baseUri(URL)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .header(header)
                .params(params)
                .get(URN)
                .then()
                .assertThat()
                .statusCode(200)
                .body("", hasSize(3))
                .log().all(true);
    }
    @Test(description = "screen_name test. Expected -> 200, for all user.screen_name == TwitterAPI ")
    public void screenNameWithCountTest() throws SignatureException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException {

        Map<String, String> params = new HashMap<>();
        params.put("screen_name", "twitterapi");
        params.put("count", "3");

        Header header = new Header("Authorization", oAuth.getHeader(method, URI, params));

        given().log().all(true)
                .baseUri(URL)
                .accept(ContentType.ANY)
                .contentType("application/x-www-form-urlencoded")
                .header(header)
                .params(params)
                .expect()
                .body("", hasSize(3))
                .body("user", everyItem(hasValue("TwitterAPI")))
                .statusCode(200)
                .log().all(true)
                .when()
                .get(URN);

    }

}
